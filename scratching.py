# Copyright (C) 2024  INRAE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-

import os
import shutil
from subprocess import Popen, PIPE

def find_cmd(lib):
    return [
        "find", "/", "-type", "f",
        "-name", f"{lib}.so*"
    ]

def find_filter(lib):
    cmd = find_cmd(lib)
    p = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=False)
    files = p.communicate()[0].split()

    files = filter(lambda p: not b"snap" in p, files)
    files = filter(lambda p: not b"flatpak" in p, files)
    files = filter(lambda p: not b"gnu/store" in p, files)
    files = filter(lambda p: b"x86_64" in p, files)
    files = list(map(lambda p: p.decode('ascii'), files))

    return files

def make_link(name):
    lib = f"{name}"

    def make_link_aux(rest):
        if len(rest) <= 1:
            return True

        ln = f"""python/lib/{".".join(rest)}"""

        print(f" LN    {ln}")
        os.symlink(lib, ln)

        return make_link_aux(rest[:-1])

    return make_link_aux(name.split(".")[:-1])

libs = [
    "libssl", #"libcrypt",
    "libcrypto",
    "libz",
    "libffi"
]

for lib in libs:
    files = find_filter(lib)

    # Copy file to python
    for f in files:
        print(f" COPY  {f}")
        name = os.path.basename(f)
        shutil.copyfile(f, f"python/lib/{name}")

        # Make symbolic link
        make_link(str(name))
